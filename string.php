<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan string</title>
</head>
<body>
<h2>Latihan PHP</h2>
<?php
    echo "<h3>Contoh soal 1</h3>";
    $kalimat1 = "Hello PHP!";
    echo "kalimat 1 : " . $kalimat1 . "<br>";
    echo "panjang string : " . strlen($kalimat1) . "<br>";
    echo "jumlah kata : " . str_word_count($kalimat1) . "<br> <br>";

    $kalimat11 = "I'm ready for the challenges";
    echo "kalimat 11 : " . $kalimat11 . "<br>";
    echo "panjang string : " . strlen($kalimat11) . "<br>";
    echo "jumlah kata : " . str_word_count($kalimat11) . "<br>";

    echo "<h3>Contoh soal 2</h3>";
    $kalimat2 = "I love PHP";
    echo "kalimat : " . $kalimat2 . "<br>";
    echo "kata 1 : " . substr($kalimat2,0,1) . "<br>";
    echo "kata 2 : " . substr($kalimat2,2,5) . "<br>";
    echo "kata 3 : " . substr($kalimat2,7,9) . "<br>";

    echo "<h3>Contoh soal 3</h3>";
    $kalimat3 = "PHP is old but Good!";
    echo "kalimat 3 : " . $kalimat3 . "<br>";
    echo "kalimat 3 di ubah : " . str_replace("Good!","awesome",$kalimat3);

?>

</body>
</html>